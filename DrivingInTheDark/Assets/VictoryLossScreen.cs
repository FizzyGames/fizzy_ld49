using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class VictoryLossScreen : MonoBehaviour
{
    private Image _screenCover;

    [SerializeField]
    private DistanceToGoalTracker _distanceToGoalTracker;
    [SerializeField]
    private Monster _monster;
    Color invisible = new Color(0, 0, 0, 0);

    private float _fadeToBlackTimer = 0.5f;
    private float _fadeToBlackTime;

    private float _fadeToWhiteTimer = 10.0f;
    private float _fadeToWhiteTime;

    [SerializeField]
    private TextMeshProUGUI _text;

    private SimpleControls _input;

    Car _car;

    private void Awake()
    {
        _input = new SimpleControls();
        _input.Enable();
        _car = FindObjectOfType<Car>();
    }

    private void Start()
    {
        _screenCover = GetComponent<Image>();
        _fadeToBlackTime = _fadeToBlackTimer;
        _fadeToWhiteTime = _fadeToWhiteTimer;
        _text.enabled = false;

    }

    private void Update()
    {
        if (_distanceToGoalTracker.CurrDistanceToGoal <= 0)
        {
            _fadeToWhiteTime -= Time.deltaTime;
            _screenCover.color = Color.Lerp(invisible, Color.white, Mathf.Clamp01(1 - _fadeToWhiteTime / _fadeToWhiteTimer));
            _text.enabled = _fadeToWhiteTime < 0;
            _text.color = Color.black;
            _text.text = "you win! press escape to quit, or spacebar to play again";
        }
        else if (_monster.MonsterTime <= 0)
        {
            _fadeToBlackTime -= Time.deltaTime;
            _screenCover.color = Color.Lerp(invisible, Color.black, Mathf.Clamp01(1 - _fadeToBlackTime / _fadeToBlackTimer));
            _text.enabled = _fadeToBlackTime < 0;
            _text.color = Color.white;
            _text.text = "press escape to quit, or spacebar to try again";
        }
        else
        {
            _screenCover.color = invisible;
        }

        if (_text.enabled)
        {
            if (_car != null)
            {
                _car.IsOverridingSpeed = true;
                _car.SpeedOverride = 0;
            }
            if (Keyboard.current.escapeKey.isPressed)
            {
                Application.Quit();
            }
            if (Keyboard.current.spaceKey.isPressed)
            {
                SceneManager.LoadScene(0);
            }
        }
    }
}
