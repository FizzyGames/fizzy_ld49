using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeAfterDelay : MonoBehaviour
{
    public float _fadeDelay = 5;
    private float _currentTimer = 0;

    // Start is called before the first frame update
    void Start()
    {
        _currentTimer = _fadeDelay;
    }

    // Update is called once per frame
    void Update()
    {
        _currentTimer -= Time.deltaTime;
        if(_currentTimer < 0)
        {
            gameObject.SetActive(false);
        }
    }
}
