using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScrollingBackgroundTilesManager : MonoBehaviour
{
    [SerializeField]
    private List<Tile> _tilesPrefabs;

    [SerializeField]
    private Car _car;

    [SerializeField]
    private Tile _startTile;

    [SerializeField]
    private Tile[] _previousTiles;
    private Tile _currentTile;
    private Tile[] neighbors;

    [SerializeField]
    private GameObject _groundTilePrefab;

    private Transform[] _groundTiles;

    private List<int> _currWeights;

    public const int s_maxTileCount = 2;

    private int _tileDepth = 4;
    private int GrassDepth => _tileDepth + 1;

    public bool _paused = false;

    private void Start()
    {
        if (_previousTiles == null || _previousTiles.Length == 0)
        {
            _previousTiles = new Tile[s_maxTileCount];
        }
        _currentTile = _startTile;
        _currentTile.GoRight = false;


        _currWeights = _tilesPrefabs.Select(x => x.Weight).ToList();

        GenerateTilesRecursive(_currentTile, _tileDepth, 0);
        _groundTiles = new Transform[GrassDepth];
        for (int i = 0; i < GrassDepth; i++)
        {
            _groundTiles[i] = Instantiate(_groundTilePrefab, new Vector3(0, 0, i * Tile.s_TileLength), Quaternion.identity).transform;
        }
    }

    //future future future
    //next - next - next
    //current
    //----

    //future future
    //next
    //current
    //----

    private void FixedUpdate()
    {
        if (!_paused)
        {
            UpdatePositionsRecursive(_previousTiles[0], _car.SidePosition);
            for (int i = 0; i < GrassDepth; i++)
            {
                _groundTiles[i].transform.position = new Vector3(-GetSidePosition(_currentTile, _car.SidePosition), -0.3f, (_currentTile.Distance + i - 1) * Tile.s_TileLength);
            }
        }

        if (_currentTile.IsDone)
        {
            foreach (Tile previousTile in _previousTiles)
            {
                if (previousTile != null)
                {
                    DestroyTile(previousTile);
                }
            }

            GenerateTilesRecursive(_currentTile, _tileDepth);

            _previousTiles = new Tile[] { _currentTile };

            //Decide which endpoint we're taking
            int decidedTile = Mathf.Clamp(_car.SidePosition > 0 ? 1 : 0, 0, _currentTile.Endpoints[0].EndpointCount - 1);

            //Destroy the other endpoint(s)
            for (int i = 0; i < _previousTiles[0].EndpointCount; i++)
            {
                if (i != (_currentTile.GoRight ? 1 : 0))
                {
                    DestroyTilesRecursive(_previousTiles[0].Endpoints[i]);
                }
            }

            _currentTile = _currentTile.Endpoints[_currentTile.GoRight ? 1 : 0];

            _currentTile.GoRight = decidedTile > 0;

            if (_currentTile.EndpointCount > 1)
            {
                StartCoroutine(DestroyTilesOnDelay(_currentTile));
            }

            _car.TargetRotation = (_currentTile.EndpointCount > 1) ? (_currentTile.GoRight ? 30 : -30) : 0;
        }

        _currentTile?.OnActiveUpdate(_car);

        IEnumerator DestroyTilesOnDelay(Tile tile)
        {
            yield return new WaitForSeconds(1);
            DestroyTilesRecursive(tile.Endpoints[tile.GoRight ? 0 : 1]);
        }

        void DestroyTilesRecursive(Tile start)
        {
            foreach (Tile tile in start.Endpoints)
            {
                if (tile != null)
                {
                    DestroyTilesRecursive(tile);
                }
            }

            DestroyTile(start);
        }

        void UpdatePositionsRecursive(Tile tile, float sidePositionOffset)
        {
            tile.Distance -= _car.Speed / Tile.s_TileLength * Time.deltaTime;
            sidePositionOffset = GetSidePosition(tile, sidePositionOffset);
            tile.SidePosition = sidePositionOffset;

            for (int i = 0; i < tile.Endpoints.Length; i++)
            {
                Tile endpoint = tile.Endpoints[i];
                if (endpoint != null)
                {
                    float offset = sidePositionOffset;
                    if (tile.EndpointCount > 1)
                    {
                        offset += (i == 0 ? 1 : -1) * Mathf.Tan(30 * Mathf.Deg2Rad) * Tile.s_TileLength;
                    }
                    UpdatePositionsRecursive(endpoint, offset);
                }
            }
        }

    }

    private static float GetSidePosition(Tile tile, float sidePositionOffset)
    {
        if (tile.EndpointCount > 1)
        {
            float clampedDist = Mathf.Clamp(1 - tile.Distance, 0, 1);
            sidePositionOffset += Mathf.Tan(30.0f * Mathf.Deg2Rad) * clampedDist * (tile.GoRight ? 1 : -1) * Tile.s_TileLength;
        }

        return sidePositionOffset;
    }

    private void DestroyTile(Tile tile)
    {
        Destroy(tile.gameObject);
    }



    private void GenerateTilesRecursive(Tile parent, int depth, int setSpawns = -1)
    {
        for (int i = 0; i < parent.EndpointCount; i++)
        {
            if (parent.Endpoints[i] == null)
            {
                parent.Endpoints[i] = GenerateTile(parent, setSpawns, (i == 0 ? null : parent.Endpoints[0]));
            }
            if (depth > 1)
            {
                GenerateTilesRecursive(parent.Endpoints[i], depth - 1, setSpawns);
            }
        }
    }


    private Tile SetUpTile(Tile toClone, Tile parent)
    {
        Tile tile = Instantiate(toClone);
        tile.Distance = parent.Distance + 1;
        return tile;
    }

    private int _antiRandomFactor = 25;

    private Tile GenerateTile(Tile parent, int setSpawn, Tile other)
    {
        if (setSpawn != -1)
        {
            return SetUpTile(_tilesPrefabs[setSpawn], parent);
        }
        //List<Tile> validTiles = _tilesPrefabs.Where(tile => tile.CanShowUpAfter(parent)).ToList();

        List<Tile> validTiles = new List<Tile>();
        List<int> tempWeights = new List<int>();

        for (int i = 0; i < _tilesPrefabs.Count; i++)
        {
            if (_tilesPrefabs[i].CanShowUpAfter(parent) && _tilesPrefabs[i].CanShowUpAlongside(other))
            {
                validTiles.Add(_tilesPrefabs[i]);
                tempWeights.Add(_currWeights[i]);
            }
        }

        int maxValidWeights = tempWeights.Sum();
        int randomTile = Random.Range(0, maxValidWeights);
        for (int i = 0; i < validTiles.Count; i++)
        {
            int rawIndex = _tilesPrefabs.FindIndex(x => x == validTiles[i]);
            if (tempWeights[i] >= randomTile)
            {
                _currWeights[rawIndex] = _tilesPrefabs[rawIndex].Weight;
                return SetUpTile(validTiles[i], parent);
            }
            _currWeights[rawIndex] += _antiRandomFactor;
            randomTile -= tempWeights[i];
        }
        Debug.Assert(false);
        return validTiles[0];
    }

    public void Pause()
    {
        //_paused = true;
        _car.IsOverridingSpeed = true;
        _car.SpeedOverride = 0;
    }

    public void Resume()
    {
        //_paused = false;
        _car.IsOverridingSpeed = false;
        _car.SpeedOverride = 0;
    }
}
