﻿using UnityEngine;

public class GasStationTile : Tile
{
    private bool _pitted;

    private float _overrideSpeed = 0;
    private float _overrideSpeedRate = 0;

    public override bool CanShowUpAfter(Tile parent)
    {
        return base.CanShowUpAfter(parent) && parent.EndpointCount > 1;
    }

    public override bool CanShowUpAlongside(Tile other)
    {
        return !(other is GasStationTile);
    }

    private float _pitStopTimer = 0.0f;


    private float _pitStopTime = 4.0f;
    private float _pitStopSlowdownTime = 1.0f;

    private bool _wasNotStopped = false;

    private void Start()
    {
        _pitted = false;
        _wasNotStopped = false;
    }

    public override void OnActiveUpdate(Car car)
    {
        base.OnActiveUpdate(car);

        if (Distance < 0.75f)
        {
            if (!_pitted)
            {
                _overrideSpeed = car.Speed;
                _pitted = true;
                _pitStopTimer = _pitStopTime;
                car.IsOverridingSpeed = true;
            }
            if (_pitStopTimer <= 0 && car.IsOverridingSpeed)
            {
                car.PlayDoorClosed();
                car.IsOverridingSpeed = false;
                car.SetRPM(0);
                car.SetFuel(1);
            }
            else
            {
                if (_overrideSpeed > 0.01f)
                {
                    _overrideSpeed = Mathf.SmoothDamp(_overrideSpeed, 0, ref _overrideSpeedRate, _pitStopSlowdownTime);
                    _pitStopTimer = _pitStopTime;
                }
                else
                {
                    if(!_wasNotStopped)
                    {
                        car.PlayDoorOpen();
                        _wasNotStopped = true;
                    }
                    _pitStopTimer -= Time.deltaTime;
                    _overrideSpeed = 0;

                    //actually stopping for gas
                    //fx here
                }

                car.SpeedOverride = _overrideSpeed;
            }
        }

    }
}