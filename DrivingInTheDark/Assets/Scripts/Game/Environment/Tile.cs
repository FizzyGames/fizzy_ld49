using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public bool GoRight { get; set; }

    public const float s_TileLength = 50;

    [SerializeField]
    private int _weight = 100;
    public int Weight => _weight;

    public virtual bool CanShowUpAfter(Tile parent)
    {
        return parent.EndpointCount <= 1 || (EndpointCount <= 1);
    }

    public virtual bool CanShowUpAlongside(Tile other)
    {
        return true;
    }

    private float _distance;
    public float Distance
    {
        get => _distance;
        set
        {
            _distance = value;
            transform.position = new Vector3(transform.position.x, transform.position.y, _distance * s_TileLength - s_TileLength / 2);
        }
    }

    public float SidePosition
    {
        set
        {
            transform.position = new Vector3(-value, transform.position.y, transform.position.z);
        }
    }

    private void Awake()
    {
        if (_endpoints == null || _endpoints.Length == 0)
        {
            _endpoints = new Tile[EndpointCount];
        }
    }

    public virtual void OnActiveUpdate(Car car) 
    {
    }

    public bool IsDone => Distance < 0;

    [SerializeField]
    private Tile[] _endpoints;

    [SerializeField]
    private int _endpointCount = 1;
    public int EndpointCount => _endpointCount;

    public Tile[] Endpoints { get => _endpoints; }
}
