using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{
    [SerializeField]
    private float _monsterTime = 250;

    public float MonsterTime => _monsterTime;

    [SerializeField]
    private float _monsterTimeMax = 250;

    [SerializeField]
    private Car _car;

    [SerializeField]
    private float _minSpeedMultiplier = 1;
    [SerializeField]
    private float _maxSpeedMultiplier = 0;

    Material _monsterEyesMaterial;

    Color _fullDarkColor = new Color(0.07f, 0.07f, 0.07f);

    private void Start()
    {
        _monsterEyesMaterial = GetComponent<Renderer>()?.material;
        _monsterTime = _monsterTimeMax;
    }

    private void FixedUpdate()
    {
        _monsterTime -= (2 - Mathf.InverseLerp(_car.MinSpeed, _car.MaxSpeed, _car.Speed)) * Time.deltaTime;
        _monsterEyesMaterial?.SetColor("_EmissionColor", Color.Lerp(Color.white, _fullDarkColor, _monsterTime / _monsterTimeMax));
        if(_monsterTime < 0)
        {
            _car.IsOverridingSpeed = true;
            _car.SpeedOverride = 0;
            _monsterTime = 0;
#if UNITY_EDITOR
            //UnityEditor.EditorApplication.ExitPlaymode();
#endif
        }
    }
}
