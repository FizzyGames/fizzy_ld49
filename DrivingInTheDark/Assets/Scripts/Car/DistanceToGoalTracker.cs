﻿using TMPro;
using UnityEngine;

public class DistanceToGoalTracker : MonoBehaviour
{
    [SerializeField]
    private Car _car;

    [SerializeField]
    private TextMeshPro _gpsReadout;

    [SerializeField]
    private Monster _monster;

    [SerializeField]
    private int _distanceToGoal = 50;


    private float _currDistanceToGoal;

    public float CurrDistanceToGoal { get => _currDistanceToGoal; set => _currDistanceToGoal = value; }
    public float CurrMetersToGoal => _currDistanceToGoal * Tile.s_TileLength;

    private void Start()
    {
        CurrDistanceToGoal = _distanceToGoal;
    }

    private void Update()
    {
        _gpsReadout.text = $"Distance Remaining:\r\n{CurrMetersToGoal.ToString("F2")}";
    }

    private void FixedUpdate()
    {
        CurrDistanceToGoal = Mathf.Max(0, CurrDistanceToGoal - _car.Speed / Tile.s_TileLength * Time.deltaTime);
        if(CurrDistanceToGoal < 0)
        {
            _monster.enabled = false;
        }
    }
}
