using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BrokenSystemController : MonoBehaviour
{
    private BreakingComponent[] _breakingComponents;
    private float[] _brokenTimers;

    [SerializeField]
    private float _brokenMinAmount = 0.01f;
    [SerializeField]
    private float _brokenMaxAmount = 2.0f;
    [SerializeField]
    private float _brokenTimerLengthMin = 0.5f;
    [SerializeField]
    private float _brokenTimerLengthMax = 10.0f;

    private void Start()
    {
        _breakingComponents = GetComponentsInChildren<BreakingComponent>();
        _brokenTimers = new float[_breakingComponents.Length];
    }

    private void Update()
    {
        for (int i = 0; i < _breakingComponents.Length; i++)
        {
            _brokenTimers[i] -= Time.deltaTime;
            if(_brokenTimers[i] < 0)
            {
                _brokenTimers[i] = Random.Range(_brokenTimerLengthMin, _brokenTimerLengthMax);
                _breakingComponents[i].BrokenAmount = Random.Range(_brokenMinAmount, _brokenMaxAmount);
            }
        }
    }
}