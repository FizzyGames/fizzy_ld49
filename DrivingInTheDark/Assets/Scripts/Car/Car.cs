using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Car : MonoBehaviour
{
    [SerializeField]
    private float _currentSpeed = 5;
    public float Speed
    {
        get => IsOverridingSpeed ? SpeedOverride : _currentSpeed;
        private set => _currentSpeed = value;
    }

    [SerializeField]
    private float _minSpeed;
    [SerializeField]
    private float _maxSpeed;

    public float MinSpeed => _minSpeed;
    public float MaxSpeed => _maxSpeed;

    [SerializeField]
    private float _offRoadMultiplier = 0.66f;

    [SerializeField]
    private float _maxWorldWidth = 20;

    [SerializeField]
    private float _sidePosition = 0.5f;

    [SerializeField]
    private float _targetSpeed = 0.0f;

    [SerializeField]
    private Transform[] _raycastPoints;

    [SerializeField]
    private LayerMask _roadCheckLayerMask;

    private float _terrainMultiplierFactor = 0;

    private float _terrainMultiplier = 1;

    [SerializeField]
    private float _terrainMultiplierDecelSpeed = 1.0f;


    RaycastHit[] _raycastHitCache = new RaycastHit[50];

    [SerializeField]
    private float _steeringSpeed = 1.0f;

    public float TargetRotation { get; set; }

    private float _currentRotation;

    private float _currentRotationVel = 0;

    [SerializeField]
    private float _rotationAdjustmentTime = 1.0f;

    public float SidePosition => _sidePosition;

    protected SimpleControls _input;

    [SerializeField]
    private float _randomSteeringTimeMax = 10.0f;
    [SerializeField]
    private float _randomSteeringTimeMin = 3.0f;
    [SerializeField]
    private float _randomSteeringMaxOffset = 0.7f;
    [SerializeField]
    private float _randomSteeringMinOffset = 0.5f;

    private float _randomSteeringTimer;
    private float _randomSteeringTimerOriginal;
    private float _randomSteeringCurrent;
    private float _randomSteeringTarget;
    private float _randomSteeringVelocity;

    [SerializeField]
    private Gauge _fuelGauge;

    public void SetRPM(float v)
    {
        _currentRPM = v;
    }

    public void SetFuel(float v)
    {
        _fuelLevel = v;
    }

    [SerializeField]
    private Gauge _rpmGauge;
    [SerializeField]
    private Gauge _speedGauge;
    [SerializeField]
    private Gauge _temperatureGauge;

    private float _idealRPM = 0.7f;
    private float _currentRPM;
    private float _idealRPMMargin = 0.05f;
    private float _rpmChangeSpeed = 0.1f;
    private float _rpmChangeTime = 3.0f;
    private float _rpmMissPenaltyRatio = 0.2f;

    [SerializeField]
    private float _currentRPMDecayRate = 0.7f;

    private float _speedAccelChangeVel;
    private float _speedAccelChangeTime;

    private float _fuelLevel = 1;
    [SerializeField]
    private AnimationCurve _fuelDecayRateBySpeed;
    [SerializeField]
    private float _fuelDecayRateMultiplier = 0.01f;

    public bool IsOverridingSpeed { get; set; }
    public float SpeedOverride { get; set; }

    public bool SteeringEnabled { get; set; }

    [SerializeField]
    private float _fuelPunishmentSpeedMultiplier = 0.3f;

    private float _steeringRotationRange = 270;

    [SerializeField]
    private Transform _steeringWheel;
    private float _smoothSteering = 0;

    [SerializeField]
    private AudioSource _carAudio;

    [SerializeField]
    private AudioClip _engineDing;

    [SerializeField]
    private float _engineDingDelay = 0.75f;
    private float _engineDingTimer = 0;

    [SerializeField]
    private AudioClip _doorOpen;
    [SerializeField]
    private AudioClip _doorClose;

    [SerializeField]
    private AudioSource _doorAudioSource;



    protected virtual void Awake()
    {
        _input = new SimpleControls();
        _input.Enable();
    }

    private void Update()
    {
        _currentRotation = Mathf.SmoothDampAngle(_currentRotation, TargetRotation, ref _currentRotationVel, _rotationAdjustmentTime);
        transform.rotation = Quaternion.Euler(0, _currentRotation, 0);

        _speedGauge.GaugePercent = Mathf.InverseLerp(0, _maxSpeed, Speed);
        _rpmGauge.GaugePercent = IsOverridingSpeed ? Mathf.InverseLerp(_minSpeed, _maxSpeed, SpeedOverride) : _currentRPM;
        _temperatureGauge.GaugePercent = IsOverridingSpeed ? 0 : CurrentPenaltyRatio;
        _fuelGauge.GaugePercent = _fuelLevel;

        _engineDingTimer -= Time.deltaTime;
        if (CurrentPenaltyRatio > 0 && !IsOverridingSpeed)
        {
            if (_engineDingTimer < 0)
            {
                _engineDingTimer = _engineDingDelay;
                _carAudio.PlayOneShot(_engineDing);
            }
        }
    }

    public void PlayDoorOpen()
    {
        _doorAudioSource.PlayOneShot(_doorOpen);
    }

    public void PlayDoorClosed()
    {
        _doorAudioSource.PlayOneShot(_doorClose);
    }

    private void FixedUpdate()
    {
        UpdateSteering();
        UpdateSpeed();
        UpdateAccel();
        UpdateFuel();
    }

    private void UpdateSteering()
    {   
        _randomSteeringTimer -= Time.deltaTime;
        if (_randomSteeringTimer < 0)
        {
            _randomSteeringTimer = _randomSteeringTimerOriginal = Random.Range(_randomSteeringTimeMin, _randomSteeringTimeMax);
            _randomSteeringTarget = Random.Range(_randomSteeringMinOffset, _randomSteeringMaxOffset) * (Random.value > 0.5f ? 1 : -1);
        }

        _randomSteeringCurrent = Mathf.SmoothDamp(_randomSteeringCurrent, _randomSteeringTarget, ref _randomSteeringVelocity, _randomSteeringTimerOriginal / 2);

        //Check inputs
        float steering = Mathf.Clamp(_input.gameplay.steer.ReadValue<float>() + _randomSteeringCurrent, -1, 1);

        if (Speed - _minSpeed < 0.05f)
        {
            steering = 0;
        }


        _smoothSteering = Mathf.MoveTowards(_smoothSteering, steering, Time.deltaTime/2);
        steering *= _steeringSpeed * Time.deltaTime;
        _steeringWheel.localRotation = Quaternion.Euler(0, Mathf.Lerp(-_steeringRotationRange, _steeringRotationRange, _smoothSteering/2 + 0.5f), 0);

        //Move the car
        _sidePosition = Mathf.Clamp(_sidePosition + steering, -_maxWorldWidth, _maxWorldWidth);
    }

    private void UpdateSpeed()
    {

        float newSpeed = _targetSpeed;

        float targetTerrainMultiplier = 1.0f;

        //Check to see if we're on the road
        foreach (Transform item in _raycastPoints)
        {
            int hitCount = Physics.RaycastNonAlloc(item.position, Vector3.down, _raycastHitCache, 50000.0f, _roadCheckLayerMask);
            if (hitCount > 0)
            {
                bool onGround = _raycastHitCache.Take(hitCount).Any(hit => hit.transform.GetComponentInChildren<Road>() != null);
                if (!onGround)
                {
                    targetTerrainMultiplier *= _offRoadMultiplier;
                }
            }
        }

        //If we're not on the road, reduce our maximum speed accordingly
        _terrainMultiplier = Mathf.SmoothDamp(_terrainMultiplier, targetTerrainMultiplier, ref _terrainMultiplierFactor, _terrainMultiplierDecelSpeed);

        //Set the speed
        Speed = Mathf.SmoothDamp(Speed, Mathf.Clamp(CurrentSpeed, _minSpeed * _terrainMultiplier, _maxSpeed * _terrainMultiplier), ref _speedAccelChangeVel, _speedAccelChangeTime);
    }

    private void UpdateAccel()
    {
        float accelInput = _input.gameplay.acceleration.ReadValue<float>();

        _currentRPM = Mathf.SmoothDamp(_currentRPM, Mathf.Clamp01(_currentRPM + accelInput), ref _rpmChangeSpeed, _rpmChangeTime);
        if (Mathf.Abs(accelInput) < 0.05f)
        { 
            _currentRPM -= _currentRPMDecayRate * Time.deltaTime;
        }
    }

    private void UpdateFuel()
    {
        _fuelLevel -= _fuelDecayRateBySpeed.Evaluate(CurrentSpeedWithoutPenalty) * _fuelDecayRateMultiplier * Time.deltaTime;
    }

    public float CurrentSpeed => Mathf.Lerp(_minSpeed, _maxSpeed, Mathf.InverseLerp(0, _idealRPM, _currentRPM) * TemperatureFactor * FuelFactor);

    public float CurrentSpeedWithoutPenalty => Mathf.Lerp(_minSpeed, _maxSpeed, Mathf.InverseLerp(0, _idealRPM, _currentRPM));

    private float TemperatureFactor => Mathf.Lerp(1.0f, _rpmMissPenaltyRatio, CurrentPenaltyRatio);

    private float CurrentPenaltyRatio => Mathf.Clamp01(Mathf.InverseLerp(_idealRPM + _idealRPMMargin, 1.0f, _currentRPM));

    private float FuelFactor => Mathf.Lerp(_fuelPunishmentSpeedMultiplier, 1, Mathf.Clamp01((_fuelLevel - 0.05f) * 20));
}
