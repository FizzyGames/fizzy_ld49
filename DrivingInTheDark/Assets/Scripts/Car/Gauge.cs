using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gauge : MonoBehaviour
{
    [SerializeField]
    private float _gaugePercent = 0;
    public float GaugePercent
    {
        get => _gaugePercent;
        set => _gaugePercent = value;
    }
    [SerializeField]
    private float _minRotation = 0;
    [SerializeField]
    private float _maxRotation = -270;

    public void Update()
    {
        transform.localRotation = Quaternion.Euler(Mathf.Lerp(_minRotation, _maxRotation, GaugePercent), 0, 0);
    }
}
