﻿using UnityEngine;

public abstract class BreakingComponent : MonoBehaviour
{
    [SerializeField]
    private float _brokenAmount;

    public float BrokenAmount
    {
        get => _brokenAmount;
        set => _brokenAmount = value;
    }

    [SerializeField]
    private float _brokenAmountBaseMultiplier = 1;

    public float BrokenAmountBaseMultiplier
    {
        get => _brokenAmountBaseMultiplier;
        set => _brokenAmountBaseMultiplier = value;
    }

    protected SimpleControls _input;


    protected virtual void Awake()
    {
        _input = new SimpleControls();
        _input.Enable();
    }
}
