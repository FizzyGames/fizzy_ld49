using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineAudio : MonoBehaviour
{
    private Car _car;

    [SerializeField]
    private AudioClip _engineStart;
    [SerializeField]
    private AudioClip _engineRunning;

    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        _car = GetComponentInParent<Car>();
    }

    // Start is called before the first frame update
    void Start()
    {
        _audioSource.PlayOneShot(_engineStart);
        StartCoroutine(PlayEngineRunningOnDelay());
    }

    IEnumerator PlayEngineRunningOnDelay()
    {
        yield return new WaitForSecondsRealtime(_engineStart.length * 0.9f);
        _audioSource.loop = true;
        _audioSource.clip = _engineRunning;
        _audioSource.Play();
    }


    // Update is called once per frame
    void Update()
    {
        _audioSource.volume = _car.Speed > 0.01f ? 1 : 0;
    }
}
